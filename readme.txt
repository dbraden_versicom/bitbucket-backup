BBB - BitBucket Backup
Version 0.3

A PHP script which backups your BitBucket repositories to AWS S3.

http://goos.dk

Copyright
Copyright GOOS web & mobile (C) 2013
	More info & inquiries <info@goos.dk>
    Code by Vladimir Bakalov <vladimir@goos.dk>

License
This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2, as published by the
Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Requirements
* PHP 5.0 or later
* (optional) Crontab

Summary
BitBucket Backup fetches your BitBucket repositories, compresses them and stores the ZIP files at Amazon's S3 service.
In the config.php file you can specify root repositories folder and all accounts info. You need to have BitBucket and AWS S3 accounts to use the script.
The script clones or pulls the latest changes from your repositories on each run.
Please note, that the script will only fetch repositories which are marked as 'watched' by your BitBucket user.
By default, the script upload a new ZIP archive for each repository every day and the archive name include the day number. This way, there will always be 28-31 archived copies of your repositories in your AWS bucket.

Download
You can get the newest version at https://bitbucket.org/goos/bitbucket-backup.

What's new
Version 0.3
- added skipping upload for unchanged or damaged repositories 

Version 0.2
- now using tar and gzip instead of ZipArchive
- code optimization
- added file extension config option

Usage
1. Download or clone the script repository in a desired location on your server
2. Edit config.php file and set your BitBucket and AWS S3 account information
3. (Optionally) Set your repositories root folder. This folder will be used to store your Git repositories before they are uploaded to AWS
4. (Optionally) Edit fetch.sh shell script file and set the full path to the fetch.php file. You can use the 'pwd' command in *nix shell to get the path.
5. Create a cron and set it to run at desired time intervals and execute the fetch.sh file.
6. Enjoy!

This product is provided without any support options and is intended to be used as-it-is.
Support

Enjoy!

